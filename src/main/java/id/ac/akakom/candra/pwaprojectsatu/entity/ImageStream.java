package id.ac.akakom.candra.pwaprojectsatu.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class ImageStream {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    private String idStream;
    private String createdBy;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    private String imagePath;
    private Integer likeCount;
    private String keterangan;

}
