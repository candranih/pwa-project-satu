package id.ac.akakom.candra.pwaprojectsatu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PwaProjectSatuApplication {

	public static void main(String[] args) {
		SpringApplication.run(PwaProjectSatuApplication.class, args);
	}

}
