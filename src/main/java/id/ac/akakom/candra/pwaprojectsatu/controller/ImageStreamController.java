package id.ac.akakom.candra.pwaprojectsatu.controller;

import id.ac.akakom.candra.pwaprojectsatu.entity.ImageStream;
import id.ac.akakom.candra.pwaprojectsatu.repository.ImageStreamRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;


@Controller
@RequestMapping("/")
public class ImageStreamController {

    @Autowired
    private ImageStreamRepository imageStreamRepository;


    @GetMapping
    public String index(Model model, Pageable pageable) {
        model.addAttribute("listData", imageStreamRepository.findAllByOrderByCreatedDateDesc(pageable));
        return "index";
    }


    @GetMapping("/upload")
    public String addPage(ImageStream imageStream,Model model) {
        return "upload";
    }


    @GetMapping("/detail")
    public String detailPage(@RequestParam String idStream,Model model) {

        model.addAttribute("data", imageStreamRepository.findByIdStream(idStream));
        return "detail";
    }



    @PostMapping("/upload")
    public String postUpload(@RequestParam String email,
                             @RequestParam String keterangan,
                             @RequestParam MultipartFile file, Model model) throws IOException {

        ImageStream imageStream = new ImageStream();
        imageStream.setCreatedDate(new Date());
        imageStream.setCreatedBy(email);
        imageStream.setKeterangan(keterangan);

        int length = 4;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        imageStream.setImagePath(saveFile(generatedString,file));

        imageStreamRepository.save(imageStream);

        return "redirect:/";

    }

    private String saveFile(String idImage, MultipartFile dokument) throws   IOException {
        String filePath = "upload/"+idImage+".jpg";
        Path path = Paths.get(filePath);
        Files.write(path, dokument.getBytes());
        return filePath;
    }

    @RequestMapping(value = "/image")
    @ResponseBody
    public void getImageFoto(@RequestParam String idStream,  HttpServletResponse response) throws IOException {
        ImageStream imageStream = imageStreamRepository.findByIdStream(idStream);
        File dir = new File(imageStream.getImagePath());
        FileInputStream inputStream = new FileInputStream(dir);
        response.setContentType("application/jpg");
        response.setContentLength((int) dir.length());
        response.setHeader("Content-Disposition", "inline;filename=\"" + dir.getName() + "\"");
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

}
