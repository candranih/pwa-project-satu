package id.ac.akakom.candra.pwaprojectsatu.repository;

import id.ac.akakom.candra.pwaprojectsatu.entity.ImageStream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ImageStreamRepository  extends PagingAndSortingRepository<ImageStream, String> {

    ImageStream findByIdStream(String idStream);

    Page<ImageStream> findAllByOrderByCreatedDateDesc(Pageable pageable);

}
